#include "filereader.h"
#include "data.h"
#include "datatable.h"
#include "report.h"

using namespace std;

int main()
{
    FileReader      reader;
    DataTable<Data> data;
    Consolidated    report;

    reader.read("data.txt", data);
    data(report);

    return 0;
}
