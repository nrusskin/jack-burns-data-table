# Formatting the raw data and preparation of custom reports 

## DESCRIPTION

### Each record contains data on one of the following construction costs
You’ll need to copy the data:
```
Lumber     LUM-11-101     M    0000    1899.35     1
Elc-lbr     ELC-22-086      L     150        5250.00   8
Plb-lbr     PLB-31-003     L    250     10750.00    9
Plb-ins     PBI-01-001     I    0000    225.00    10
Electr     ELC-44-192     E    0000    633.49      15
Lumber    LUM-22-202     M    0000    25325.00    11
Transp    CON-01-100   C    0000    614.01      19
Grounds    LAN-14-400   L    125     3187.50     43
Sod-bak    LAN-14-401   M    0000    1760.05     42
Sod-frt    LAN-14-402   M    0000    4777.75     42
Frm-lbr    FRM-02-100   L     795     19477.50    25
Elc-lbr    ELC-22-095    L    235     8225.00     39
Shingls    EXT-13-003   M    0000    4799.00     27
Lumber     LUM-33-303   M  0000  2405.00   40
Flr-lbr    FLR-20-230   L    160     2080.00     52
Elc-ins    ELI-01-001   I    0000    495.00       82
Soil-ts    CON-02-200   C     0000    179.00      93
Pol-mat    POL-32-004   M    0000    14900.00    101
Pol-lbr    POL-32-005   L    360     3240.00     120
Pol-fin    POL-32-006   L    119     1368.50     129
Pol-htr    POL-32-007   M    0000    1550.00     128
```


### Each record contains data on one of the following construction costs, in the following record format.

## DATA TYPES NEEDED
* Item name string Max 20 chars Lumber, SOD, Plb-labor, Elc-labor
* Item code string 10 character contract code LUM-01-302, PLB-21-130, ELC-49-127
* Item type character 1-character code type ‘M’, ‘L’, ‘I’, ‘C’, ‘E’
* Hours float Manhours labor (or 0000 if flat cost) 437.0 (manhours for this item)
* Charge float Actual cost of item 1999.32 (dollars and cents)
* Day int Which day item was completed Day one was first day of project


## OUTPUT

### For each record, print the following on one line: 
* line item number; 
* item contractor code; 
* item name;
* type of charge (interpret the character and print MATLS for materials; LABOR for labor; INSPC for inspections; CONTR for a contractor costs; or ELECT for electrical);
* Labor cost per hour, only if appropriate (don’t print zeroes!); 
* The actual cost of the item.

### Print column headers at the top of the entire display so the table looks likes, well, a table. (The data base is small enough so that the entire table should fit on one screen.) The listing should look something like this:
```
# CONTR CODE ITEM NAME TYPE COST/HOUR ITEM COST
1 LUM-01-101 Lumber MATLS 15290.00
2 ELC-22-086 Elc-labor LABOR 35.00 5250.00
3 PLB-31-003 Plb-labor LABOR 43.00 10750.00
4 PBI-01-001 Plb-insp INSPC 225.00
```
(NOTE: dollar items in two decimal places and lined up right-justified.)

To print the type of charge, use a separate function to test the character code and print the appropriate type in the proper column. Also, use a separate function to compute the labor cost per hour, but don’t print it inside the function. (The use of these two functions will reduce the size of your loop considerably! The use of other functions is not mandatory)


## REPORTING

When you’ve finished processing individual construction item, print out a polished final summary (make it look like a contractor’s report). Report the following:

* Number of items included in the database
* Total cost of all lumber
* Total man-hours charged and total labor cost overall
* Total of all charges

## RESULT
```
#  CONTR CODE	ITEM NAME	TYPE	COST/HOUR	ITEM COST
-----------------------------------------------------------------
1 LUM-11-101	Lumber		MATLS	                 15194.80
2 ELC-22-086	Elc-lbr		LABOR	35.00             5250.00
3 PLB-31-003	Plb-lbr		LABOR	43.00            10750.00
4 PBI-01-001	Plb-ins		INSPC	                 18000.00
5 ELC-44-192	Electr		ELECT	                 76018.80
6 LUM-22-202	Lumber		MATLS	               2228600.00
7 CON-01-100	Transp		CONTR	                 93329.52
8 LAN-14-400	Grounds		LABOR	25.50             3187.50
9 LAN-14-401	Sod-bak		MATLS	                591376.82
10 LAN-14-402	Sod-frt		MATLS	               1605324.00
11 FRM-02-100	Frm-lbr		LABOR	24.50            19477.50
12 ELC-22-095	Elc-lbr		LABOR	35.00             8225.00
13 EXT-13-003	Shingls		MATLS	               1036584.00
14 LUM-33-303	Lumber		MATLS	                769600.00
15 FLR-20-230	Flr-lbr		LABOR	13.00             2080.00
16 ELI-01-001	Elc-ins		INSPC	                324720.00
17 CON-02-200	Soil-ts		CONTR	                133176.00
18 POL-32-004	Pol-mat		MATLS	              12039200.00
19 POL-32-005	Pol-lbr		LABOR	 9.00             3240.00
20 POL-32-006	Pol-fin		LABOR	11.50             1368.50
21 POL-32-007	Pol-htr		MATLS	               1587200.00
-----------------------------------------------------------------
Total cost of all lumber:..............................3013394.75
Total man-hours charged:..................................2194.00
Total labor cost overall:................................53578.50
Total charges:........................................20571902.00
```
