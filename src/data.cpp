#include "data.h"

int  Data::_ID_ = 0;

std::ostream& operator << (std::ostream& stream, const Data& data)
{
    stream.precision(2);
    stream.setf( std::ios::fixed, std:: ios::floatfield );
    return data.print(stream);
}
