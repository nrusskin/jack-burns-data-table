#ifndef FILEREADER_H
#define FILEREADER_H

#include <string>
#include <fstream>
#include <iostream>

// File system wrapper
class FileReader
{
    public:
        // read raw file
		template <typename Table>
		void read(const std::string& filename, Table& table) const
		{
            // open source file
			std::filebuf fb;
			if (fb.open (filename.c_str(), std::ios::in))
			{
				std::istream is(&fb);
				std::string row;
                // read file line by line
				while (std::getline(is, row))
					if (!row.empty())
                        // create new database row (see DataTable::add)
						table.add(row);
				fb.close();
			} else {
				std::cerr << "Failed to open file " << filename << std::endl;
			}
		}
};

#endif // FILEREADER_H
