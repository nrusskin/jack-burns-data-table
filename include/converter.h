#ifndef CONVERTER_H
#define CONVERTER_H

#include <cstdlib>
#include <string>

// convert std::string to different types (see Parser::chunk)
template <typename TargetType> struct Converter
{
    template <typename SourceType>
    TargetType operator() (const SourceType& data) const{ return data;}
};

template <> struct Converter <int> { int operator() (const std::string& data) { return atoi(data.c_str());}};
template <> struct Converter <float> { float operator() (const std::string& data) { return atof(data.c_str());}};
template <> struct Converter <char> { float operator() (const std::string& data) { return data[0];}};

#endif // CONVERTER_H
