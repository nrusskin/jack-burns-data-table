#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <sstream>
#include "converter.h"

// parse one row of input file
class Parser
{
public:
    Parser(const std::string& line) : data(line) {}

    // extract one chunk and convert to target type
    template <typename T>
    T chunk()
    {
        Converter<T> cnv;
        return cnv(_chunk());
    }

private:
    // extract next chunk from the line
    std::string _chunk()
    {
        std::string out;
        std::istringstream  stream(data);
        std::getline(stream, out, ' ');
        std::getline(stream, data);
        trim(data);
        return out;
    }
    // trim spaces
    void trim(std::string& line)
    {
        std::size_t start = line.find_first_not_of(" \t\f\v\n\r");
        if (start!=std::string::npos)
            line.erase(0,start);
    }

    std::string data;
};

#endif//PARSER_H
