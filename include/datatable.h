#ifndef DATATABLE_H
#define DATATABLE_H

#include <list>
#include <string>
#include <iostream>

// database table implementation
template <typename T>
class DataTable
{
    public:
        DataTable(){};

        // create new row (see FileReader)
        typedef std::string Line;
        void add(const Line& line)
        {
            rows.push_back(T(line));
        }

        // enumerate all rows (see Reporter)
        template <typename Fn>
        void operator () (Fn& fn)
        {
            for(auto& row: rows)
                fn(row);
            fn.report();
        }
    private:
        // storage of rows
        std::list<T> rows;
};

#endif // DATATABLE_H
