#ifndef REPORT_H_INCLUDED
#define REPORT_H_INCLUDED

#include <iostream>
#include <algorithm>
#include <list>
#include "data.h"

// Base report class
class Report
{
public:
    Report() : total(0) {}
    virtual ~Report(){}
    // enumerate each rows
    virtual void operator() (Data& data)
    {
        // matching some conditions
        if (test(data))
            total += data.getCharges();
    }
    virtual void report() const{};
protected:
    // condition - nobody rows (by default)
    virtual bool test (Data& data) {return false;}
    void print(int width, float data) const
    {
        std::cout.width(width);
        std::cout.fill('.');
        std::cout << data << std::endl;
    }
    float total;
};

// print table to console
class Printer: public Report
{
public:
    Printer()
    {
        std::cout << "#  CONTR CODE\tITEM NAME\tTYPE\tCOST/HOUR\tITEM COST" << std::endl;
        fill(std::cout);
    }
    virtual void operator() (Data& data) { std::cout << data << std::endl; }
    virtual void report() const { fill(std::cout); }
private:
    void fill(std::ostream& stream) const
    {
        stream.width (65);
        stream.fill('-');
        stream<<"\0" << std::endl;
        stream.fill(' ');
    }
};

// calculate total cost of all lumber
class Lumber: public Report
{
public:
    virtual void report() const { std::cout <<"Total cost of all lumber:"; print(40, total); }
protected:
    virtual bool test (Data& data) {return data.getName() == "Lumber";}

};

// calculate total charges
class Total: public Report
{
public:
    virtual void report() const { std::cout<<"Total charges:"; print(51, total); }
protected:
    // any rows
    virtual bool test (Data& data) {return true;}
};

//calculate total man-hours
class HoursCharged: public Report
{
public:
    HoursCharged() : hours(0) {}
    virtual void report() const { std::cout<<"Total man-hours charged:"; print(41, hours); }
    virtual void operator() (Data& data) { hours += data.getHours(); }
private:
    int hours;
};

//calculate total labor cost
class LaborCost: public Report
{
public:
    virtual void report() const { std::cout<<"Total labor cost overall:"; print(40, total); }
protected:
    // only labor
    virtual bool test (Data& data) {return data.getType() == 'L';}
};

// consolidated report
class Consolidated: public Report
{
public:
    // create specific reporters
    Consolidated()
    {
        reports.push_back(new Printer);
        reports.push_back(new Lumber);
        reports.push_back(new HoursCharged);
        reports.push_back(new LaborCost);
        reports.push_back(new Total);
    }
    // free memory
    virtual ~Consolidated() { for(auto& report: reports) delete report; }
    // enumerate rows
    virtual void operator() (Data& data) { for(auto& report: reports) (*report)(data); }
    // print all reports
    virtual void report() const { for(auto& report: reports) report->report(); }
private:
    // report storage
    std::list<Report*>  reports;
};

#endif // REPORT_H_INCLUDED
