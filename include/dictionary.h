#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <map>
#include <string>
#include <iostream>

// replace 'L' to "LABOR", 'M' to "MATLS" etc
class Dictionary
{
    public:
        Dictionary();

        // access by key
        typedef std::map<char, std::string> tDictionary;
        const std::string& operator[] (const char key) const
        {
            auto findKey = dict.find(key);
            if (findKey == dict.end())
                std::cerr << "Key (" << key << ") not found" << std::endl;
            return dict.at(key);
        }
    private:
        tDictionary dict;
};

#endif // DICTIONARY_H
